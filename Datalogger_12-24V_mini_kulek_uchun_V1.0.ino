/**************************************************************
   Proyekt:  Data Logger (Hava stansiyasi ucun)

   Tapsiriq: Hava stansiyasindan periodic gelen datani 5 deqiqe
             periodla HTTP Post metodu ile servere gondermek

   Muellif:  Majid AHMADOV

   Hedefler: SIM800 modulunu her zaman aktiv saxlamaq. Bunun ucun
             davamli status pinini yoxlayib lazim geldikde PwrKey
             pininden istifade etmek lazimdir (TAMAMLANDI)

             Sim kartin yerinde oldugunu yoxlamaq (TAMAMLANDI)

             Hava stansiyasindan gelen melumati toplamaq(TAMAMLANDI)

             Http Post melumatini hazirlamaq(TAMAMLANDI)

             Http Post funksiyasi hazirlamaq(TAMAMLANDI)

             Hava Stansiyasinin datasini izah etmek (Server terefi
             ucun)

             Sisteme WatchDog elave etmek(TAMAMLANDI)

             3 defe ardicil data gondermekde problem yasanarsa
             reset at(TAMAMLANDI)

  QEYDLER:
     CIHAZI ISE SALMAZDAN EVVEL ASAGIDAKILARI 'MUTLEQ' OXUYUN
  1) #define HTTP_ADDRESS setirine istediyiniz http adresi yazin

  2) #define SEND_PERIOD setirine istediyiniz data gonderme periodunu yazin

  3) #define APN setirine apn adini yazin

  4) #define AT_COMMAND_PRINT setirini eger at komanda cavablarini gormek
     isteyirsizse aktiv edin. (yeni komentden qurtarin)

  5) CIHAZ BASLARKEN SIM800 MODULUNDAN CAVAB ALA BILMESE MONITORDA BUNU
     BILDIRECEK VE KOD DAVAM ETMEYECEK

  6) CIHAZ BASLARKEN EGER SIM KARTIN TAXILDIGINI GORMESE MONITORDA BUNU
     BILDIRECEK VE KOD DAVAM ETMEYECEK

  7) EGER 3 DEFE HTTP POST KOMANDASI UGURSUZ OLARSA SISTEM RESTART ATACAQ.
     HTTP POST KOMANDSININ UGURSUZ OLMAGINA SEBEBLER ASAGIDAKILAR OLA BILER:
     A)IMEI QEYDIYATSIZDIR
     B)SIM800 YAXSI QIDALANMIR
     C)SIGNAL YAXSI CEKMIR
     D)SIM800 ANTENI TAXILMAYIB VEYA PROBLEMLIDIR
     E)APN DUZGUN AYARLANMAYIB
     F)NOMRE AKTIV DEYIL VE YA KONTURU YOXDUR

  8) HTTP POST DATASI
     EGER HAVA STANSIYASINDAN DATA GELERSE:
     imei=<IMEI>,ws_data=<HAVA_STANSIYASI_DATASI>
     NUMUNE: imei=862273045802022,ws_data=247872627145000000080002000000A3D30184068B

     EGER HAVA STANSIYASINDAN YENI DATA ALINMAYIBSA:
     imei=<IMEI>,ws_data=ERROR
     NUMUNE: imei=862273045802022,ws_data=ERROR
 **************************************************************/

#include <avr/wdt.h>

//-------------------------------------------------------------//
//                           SIM 800                           //
//-------------------------------------------------------------//
HardwareSerial & sim800 =       Serial1;

#define HTTP_ADDRESS            "http://test.sumaks.com/unknown/"
#define SEND_PERIOD             300 //saniye ile
#define APN                     "internet" //azercell ucun=> "internet"
//#define AT_COMMAND_PRINT        //bunu acib baglamaqla sim800 at komandlari ve cavablarini gostermek veya gizlemek olar

#define PIN_SIM_STATUS          23
#define PIN_SIM_PWRKEY          28

#define SIM_POWER_STATUS()      digitalRead(PIN_SIM_STATUS)
#define SIM_PWRKEY_H()          digitalWrite(PIN_SIM_PWRKEY,HIGH)
#define SIM_PWRKEY_L()          digitalWrite(PIN_SIM_PWRKEY,LOW)

/*sim800 uart buffer*/
char rx_buffer[4096];

/*cihaz imei*/
char IMEI[16];

#define SIM_POWER_ON()          {\
    if(!SIM_POWER_STATUS()) {\
      printf("Sim800 POWER ON\r\n");\
      SIM_PWRKEY_H(); delay(1500); \
      SIM_PWRKEY_L(); delay(300); \
      for (char i = 0; i < 7; i++) {\
        printf("Sim800 is veziyyetine hazirlanir, %d saniye...\n", i); \
        delay(1000); \
        wdt_reset();\
      }\
    }\
  }
#define SIM_POWER_OFF()         {\
    if(SIM_POWER_STATUS()) {\
      SIM_PWRKEY_H(); delay(1500); \
      SIM_PWRKEY_L(); delay(1000); \
      printf("Sim POWER OFF\n");\
    }\
  }


//-------------------------------------------------------------//
//                           DEBUG                             //
//-------------------------------------------------------------//
#define DBG_BUF_LEN   256
char DBG_BUFFER[DBG_BUF_LEN];
#define printf(FORMAT,...) {\
    memset(DBG_BUFFER, 0, DBG_BUF_LEN);\
    sprintf(DBG_BUFFER,FORMAT,##__VA_ARGS__); \
    Serial.print(DBG_BUFFER);\
  }


//-------------------------------------------------------------//
//                     HAVA STANSIYASI                         //
//-------------------------------------------------------------//
HardwareSerial & ws =                Serial3;
#define PIN_RS485_REDE_WS            9
#define ws_receive_mode_enable()     digitalWrite(PIN_RS485_REDE_WS,LOW)
#define ws_transmit_mode_enable()    digitalWrite(PIN_RS485_REDE_WS,HIGH)

char ws_rx_buffer[256];



/*arduino'ya reset atmaq ucun*/
void(* reset_arduino) (void) = 0;

/*-----------------------------------------------------------------------------------------------------------------------------*/
void setup() {
  /*watchdog aktiv et (8 saniye)*/
  wdt_enable(WDTO_8S);

  /*timer 1-i 1 saniye perioda ayarla*/
  timer_1_init();

  Serial.begin(9600);
  sim800_init();
  weather_station_init();
}


void loop() {
  /* watchdog resetle*/
  wdt_reset();

  /*her zaman Sim800 - u islek veziyyete saxlamaq ucun*/
  if (!SIM_POWER_STATUS()) { //eger sonuludurse
    printf("Sim800 sonuludur. Sistem yeniden basladilacaq...\r\n");
    reset_arduino();
  }

  /*HAVA STANSIYAYSI DATASINI OXU*/
  static uint8_t new_data_flag = 0;
  int8_t ret_val = 0;
  ret_val = weather_station_data_handler();
  if (ret_val == 0) //eger yeni data gelibse bunu bildir
    new_data_flag = 1;

  /*melumatlari http post ile servere gonder*/
  static uint32_t time_send_data = 0;
  if (((uint32_t)(second() - time_send_data) > (SEND_PERIOD))) {
    time_send_data = second();
    char http_post_data[256];
    if (new_data_flag > 0) {
      snprintf(http_post_data, 256, "imei=%s,ws_data=[%s]", "862273045802022", ws_rx_buffer);
      new_data_flag = 0;
    } else {
      snprintf(http_post_data, 256, "imei=%s,ws_data=error", "862273045802022");
    }
    printf("HTTP POST DATA: %s\r\n", http_post_data);
    ret_val = http_post(http_post_data, strlen(http_post_data));
    printf("HTTP POST = %s\r\n", (ret_val == 0 ? "OK" : "ERROR"));

    static uint8_t error_counter = 0;
    if (ret_val < 0) {
      error_counter++;
    } else {
      error_counter = 0;
    }

    if (error_counter >= 3) {
      printf("3 defe HTTP POST ugursuz oldu. Sistem yeniden basladilacaq...\r\n");
      SIM_POWER_OFF();
      reset_arduino();
    }
  }
}

/*-----------------------------------------------------------------------------------------------------------------*/
/*
  at komandalari sonuna \r\n elave etmeye ehtiyac olmadan gonder.
  at_command => gonderilen at komanda (mes, "AT")
  desired_response => gozlenilen cavab (mes, "OK")
  timeout => maksimum cavabi gozleme muddeti
*/
int8_t send_at_command(char* at_command, char* desired_response, uint32_t timeout)
{
  /* watchdog resetle*/
  wdt_reset(); //bypass elemek ucun

  sim800.write(at_command);
  sim800.write("\r\n");

  uint32_t time_start = 0;
  uint8_t timer_counter = 0;
  memset(rx_buffer, 0, sizeof(rx_buffer));

  for (uint16_t i = 0; time_start <= timeout; ) {
    while (sim800.available() > 0) {
      char ch = sim800.read();
      if (i < (sizeof(rx_buffer) - 1))
        rx_buffer[i] = ch;
      i++;
      delay(2);
      time_start += 2;
    }
    delay(8);
    time_start += 8;

    if (sim800_uart_handler(desired_response) == 0) {
#ifdef AT_COMMAND_PRINT
      printf("\n-------AT COMMAND-------\n");
      printf("%s", rx_buffer);
      printf("------------------------\n\n");
#endif
      return 0;
    }

    if (time_start / 5000 > timer_counter) { //her 5 saniyede bir watchdog bypass ele
      timer_counter++;
      if (timer_counter < 24) { //120 saniye maximum
        /* watchdog resetle*/
        wdt_reset(); //bypass elemek ucun
      }
    }
  }
#ifdef AT_COMMAND_PRINT
  printf("\n-------AT COMMAND-------\n");
  printf("%s", rx_buffer);
  printf("------------------------\n\n");
#endif
  return -1;
}

/*
  sim800-den gelen cavablari yoxlamaq ucun funksiya
*/
int8_t sim800_uart_handler(char* desired_response)
{
  char *ptr;


  /*burada istenilen neticeleri goturmek olar*/
  if (strstr(rx_buffer, "AT+GSN")) {
    ptr = strchr(rx_buffer, '\n');
    ptr++;
    snprintf(IMEI, 16, ptr);
  }

  /*eger cavab alinbsa bildir*/
  //  Serial.println("buf = " + String(rx_buffer) + "\nsearch = " + String(desired_response));
  if (strstr(rx_buffer, desired_response)) {
    return 0;
  }

  return -1;
}

/*
  sim800 imeisini oxumaq ucun funksiya
*/
void imei_read()
{
  memset(IMEI, 0, sizeof(IMEI));
  send_at_command("AT+GSN", "OK", 1000);
  printf("IMEI = %s\r\n", IMEI);
}

/*
  sim800 cihazini baslatmaq ucun funksiya
  sim800-u aktif edir + simkartin taxildigini yoxlayir + imeini oxuyur
*/
void sim800_init(void)
{
  pinMode(PIN_SIM_STATUS, INPUT);
  pinMode(PIN_SIM_PWRKEY, OUTPUT);

  SIM_POWER_OFF();
  sim800.begin(9600);

  SIM_POWER_ON();

  /*eger sim800 xeberlesmesinde problem vardisa bildir*/
  int8_t ret_val = send_at_command("AT", "OK", 500);
  while (ret_val < 0) {
    printf("UART COMMUNICATION ERROR\n");
    delay(1000);
  }

  /*sim karti yoxla*/
  ret_val = send_at_command("AT+CSMINS?", "+CSMINS: 0,1", 1000);
  while (ret_val < 0) {
    printf("SIM KART TAXILI DEYIL!. KARTI TAX VE CIHAZI SONDUR-YANDIR!\n");
    delay(1000);
  }

  /*imeini oxu*/
  imei_read();

  /*apn ayarla*/
  send_at_command("AT+SAPBR=3,1,\"Contype\",\"GPRS\"", "OK", 1500);
  char at_apn[128];
  snprintf(at_apn, 128, "AT+SAPBR=3,1,\"APN\",\"%s\"", APN);
  send_at_command(at_apn, "OK", 1500);
}


int8_t http_post(char *msg, uint16_t msg_len)
{
  int8_t ret = 0;
  /*start connection*/
  ret |= send_at_command("AT+SAPBR=1,1", "OK", 1500);

  /*get local ip*/
  ret |= send_at_command("AT+SAPBR=2,1", "OK", 1500);

  /*init http*/
  char at_http[256];
  snprintf(at_http, 256, "AT+HTTPPARA=\"URL\",\"%s\"", HTTP_ADDRESS);
  ret |= send_at_command("AT+HTTPINIT", "OK", 1500);
  ret |= send_at_command("AT + HTTPPARA = \"CID\",1", "OK", 1500);
  ret |= send_at_command(at_http, "OK", 1500);
  ret |= send_at_command("AT+HTTPPARA=\"CONTENT\",\"application/json\"", "OK", 1500);

  /*send http data*/
  snprintf(at_http, 256, "AT+HTTPDATA=%d,15000", msg_len);
  ret |= send_at_command(at_http, "DOWNLOAD", 15000);
  ret |= send_at_command(msg, "OK", 5000);

  /*post data*/
  ret |= send_at_command("AT+HTTPACTION=1", "+HTTPACTION:", 10000);
  //  delay(5000);

  /*terminate http service*/
  ret |= send_at_command("AT+HTTPTERM", "OK", 1500);

  /*end connection*/
  ret |= send_at_command("AT+SAPBR=0,1", "OK", 1500);

  return ret;
}




/*
  Hava stansiyasini baslatmaq ucun funksiya
  rs485-i receive moda ayarlayir
*/
void weather_station_init(void)
{
  pinMode(PIN_RS485_REDE_WS, OUTPUT);
  ws.begin(9600);
  ws_receive_mode_enable();

}


int8_t weather_station_data_handler(void)
{String s;
  static uint8_t new_data = 0;
 // char hex_buffer[3];

  if (ws.available()) {
      memset(ws_rx_buffer, 0, sizeof(ws_rx_buffer));
      s = "";
    while (ws.available()) {
      
     s = ws.readStringUntil('\n');
     s.toCharArray(ws_rx_buffer,sizeof(ws_rx_buffer));
      delay(2);
      new_data = 1;
    }
  }
  if (new_data == 1) {
    printf("hava stansiyasi = %s\r\n", ws_rx_buffer);
    new_data = 0;
    return 0;
  }
  return -1;
}

/*timer 1 init*/
void timer_1_init()
{
  noInterrupts();// interruptlari dayandir
  TCCR1A = B00000000;
  TCCR1B = B00001100; //16MHz/256=62.5KHz
  TIMSK1 = B00000010; //OCR1A match
  OCR1A  = (unsigned long)(62500 - 1);
  interrupts();//interruptlar davam etsin
}

/*timer1  interrupt callback*/
volatile uint32_t seconds = 0;
ISR(TIMER1_COMPA_vect) {
  seconds++;
}

uint32_t second()
{
  return seconds;
}
