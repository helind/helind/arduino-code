#include "stdlib.h"
#include "U8g2lib.h"
#include "ArduinoModbus.h"
#include "avr/wdt.h"
#include "HELIND_LCD.h"
#include "EEPROM.h"

#define menu 21
#define up 20
#define down 19
#define OK 18
#define backlight 37

#define maxVolt max_min_array[0]
#define minVolt max_min_array[1]
#define maxSpd max_min_array[2]
#define minSpd max_min_array[3]

#define enablePin 9
#define charging 31
#define braking_pin 32
#define phase_braking_pin 30
#define braking_led 23
#define phase_break_led 24
#define charging_led 22
#define fan 28
#define ThermistorPin
#define mode_switch 33
#define termPin 0
#define battery_voltage A1

char speed_string[10];
char battery_string[10];
char percent_string[10];
short speed = 0;
bool braking = false;
bool phase_braking = false;
unsigned long time;
unsigned long period=0;

float voltage;
float volthigh, voltlow;
float voltage_percent;
float max_volt, min_volt, max_speed, min_speed;
float temperaturee;

int8_t sensor_save = 0;
volatile int8_t store = 0;
uint8_t battery_level;
int8_t sensor_error_count = 10;
int b2 = 37;
int n;
int eeaddress;
int count = 0;
uint8_t v24 = 0;
long debouncing_time = 300; //Debouncing Time in Milliseconds
volatile unsigned long last_micros;
bool sensorState = false;
bool flash = false;


int Vo;
float R1 = 10000;
float logR2, R2, T, Tc, Tf;
float c1 = 1.009249522e-03, c2 = 2.378405444e-04, c3 = 2.019202697e-07;

void wind_turbine_performance()
{
  u8g.setFontPosBaseline();
  u8g.setFontMode(0);
  u8g.setDrawColor(1);
  u8g.setFont(u8g2_font_6x12_tr);
  u8g.drawXBMP( 0, 0, 32, 32, wind1); u8g.drawXBMP( 45, 15, 25, 28, battery);
  if(flash){
  if(voltage<=11.9)u8g.drawStr(110,10,"E0");
if(!sensorState&&wind_sensor)u8g.drawStr(110,20,"E1");}
  switch (battery_level) {
    case 1: u8g.drawXBMP( 47, b2, 21, 3, battery2); break;
    case 2: u8g.drawXBMP( 47, b2, 21, 3, battery2); u8g.drawXBMP( 47, 33, 21, 3, battery2); break;
    case 3: u8g.drawXBMP( 47, b2, 21, 3, battery2); u8g.drawXBMP( 47, 33, 21, 3, battery2); u8g.drawXBMP( 47, 29, 21, 3, battery2); break;
    case 4: u8g.drawXBMP( 47, b2, 21, 3, battery2); u8g.drawXBMP( 47, 33, 21, 3, battery2); u8g.drawXBMP( 47, 29, 21, 3, battery2); u8g.drawXBMP( 47, 25, 21, 3, battery2); break;
    case 5: u8g.drawXBMP( 47, b2, 21, 3, battery2); u8g.drawXBMP( 47, 33, 21, 3, battery2); u8g.drawXBMP( 47, 29, 21, 3, battery2); u8g.drawXBMP( 47, 25, 21, 3, battery2);
      u8g.drawXBMP( 47, 21, 21, 3, battery2); break;
  }

  if (braking == true)u8g.drawXBMP( 90, 11, 27, 13, resistor1);
  if (phase_braking == true)u8g.drawXBMP( 90, 25, 25, 27, resistor3);
  if (wind_sensor) {
    u8g.drawStr( 30, 10, "Speed:");
    u8g.drawStr( 67, 10, speed_string);
    if(sensorState)u8g.drawStr( 85, 10, "m/s");
  }
  u8g.drawStr( 5, 60, "voltage"); u8g.drawStr( 53, 60, battery_string); u8g.drawStr( 80, 60, "V");
  u8g.drawStr( 97, 60, percent_string); u8g.drawStr( 122, 60, "%");

}


void setup() {
  uiSetup_page1();
  Serial.begin(9600);
  Serial2.begin(9600);
  digitalWrite(enablePin, HIGH);
  pinMode(backlight, OUTPUT);
  digitalWrite(backlight, LOW);
  pinMode(charging, OUTPUT);
  pinMode(braking_pin, OUTPUT);
  pinMode(phase_braking_pin, OUTPUT);
  pinMode(charging_led, OUTPUT);
  pinMode(braking_led, OUTPUT);
  pinMode(phase_break_led, OUTPUT);
  pinMode(mode_switch, OUTPUT);
  pinMode(fan, OUTPUT);
pinMode(25,OUTPUT);
pinMode(26,OUTPUT);
  pinMode(menu, INPUT_PULLUP);
  pinMode(up, INPUT_PULLUP);
  pinMode(down, INPUT_PULLUP);
  pinMode(OK, INPUT_PULLUP);

  attachInterrupt(digitalPinToInterrupt(menu), menu_clicked, RISING);
  attachInterrupt(digitalPinToInterrupt(up), up_clicked, RISING);
  attachInterrupt(digitalPinToInterrupt(down), down_clicked, RISING);
  attachInterrupt(digitalPinToInterrupt(OK), OK_clicked, RISING);



  if (!ModbusRTUClient.begin(9600))
  {
    Serial.println("MODBUS CLIENT ucun kommunikasiya ugurlu olmadi!");
    while (1);
  }
  EEPROM.get(16, v24);

  if (v24 - (digitalRead(mode_switch) == HIGH) == 0) {
    eeaddress = 0;
    EEPROM.get(eeaddress, max_min_array[0]);
    eeaddress += sizeof(float);
    EEPROM.get(eeaddress, max_min_array[1]);
    eeaddress += sizeof(float);
    EEPROM.get(eeaddress, max_min_array[2]);
    eeaddress += sizeof(float);
    EEPROM.get(eeaddress, max_min_array[3]);
    eeaddress += sizeof(int);
    EEPROM.get(eeaddress, sensor_save);
  }
  else

    if (digitalRead(mode_switch) == LOW)
    {
      if (isnan(max_min_array[0]) || isnan(max_min_array[1]) || isnan(max_min_array[2]) || isnan(max_min_array[3]) )
      {
        max_min_array[0] = 16.0; // max voltage: 16 V
        max_min_array[1] = 9.0; //  min voltage: 9 V
        max_min_array[2] = 17; //wind speed max :17 m/s
        max_min_array[3] = 5; //wind speed max :5 m/s
      }
      else if (max_min_array[0] == 0.0 || max_min_array[1] == 0.0 || max_min_array[2] == 0.0 || max_min_array[3] == 0.0)
      {
        max_min_array[0] = 16.0; // max voltage: 16 V
        max_min_array[1] = 9.0; //  min voltage: 9 V
        max_min_array[2] = 17; //wind speed max :17 m/s
        max_min_array[3] = 5; //wind speed max :5 m/s
      }
    }

    else

      if (isnan(max_min_array[0]) || isnan(max_min_array[1]) || isnan(max_min_array[2]) || isnan(max_min_array[3]) )
      {
        max_min_array[0] = 30; // max voltage: 30 V
        max_min_array[1] = 20; //  min voltage: 20 V
        max_min_array[2] = 17; //wind speed max :17 m/s
        max_min_array[3] = 5; //wind speed max :5 m/s
      }
      else if (max_min_array[0] == 0.0 || max_min_array[1] == 0.0 || max_min_array[2] == 0.0 || max_min_array[3] == 0.0)
      {
        max_min_array[0] = 30; // max voltage: 30 V
        max_min_array[1] = 20; //  min voltage: 20 V
        max_min_array[2] = 17; //wind speed max :17 m/s
        max_min_array[3] = 5; //wind speed max :5 m/s
      }


  if (sensor_save != 0 && sensor_save != 1) sensor_save = 0;
  else if (sensor_save == 1) wind_sensor = true;
  else wind_sensor = false;




}

void loop() {

  wdt_enable(WDTO_8S);
  time = millis();
  tempCheck();
  if (sensor_error_count < 0) {
    sensor_error_count = 10;
  }
  if (!wind_sensor) {
    memset(speed_string, 0, sizeof(speed_string));
    speed_string[0] = 'o';
    speed_string[1] = 'f';
    speed_string[2] = 'f';
  }
  if (wind_sensor)
  {

    if (!ModbusRTUClient.requestFrom(1, HOLDING_REGISTERS, 0x00, 1))
    {
      Serial.println(ModbusRTUClient.lastError());
      sensor_error_count++;
            digitalWrite(25,LOW);
      digitalWrite(26,LOW);
      if (sensor_error_count > 10) {
        speed_string[0] = 'E';
        speed_string[1] = '1';
        speed_string[2] = 0;
        sensorState = false;

      }
    }
    else
    {
      sensorState = true;
      sensor_error_count = 0;
      speed = ModbusRTUClient.read();
      speed = speed / 10;
      digitalWrite(25,HIGH);
      digitalWrite(26,HIGH);
      itoa(speed, speed_string, 10);
    }
  }


  if (wind_sensor)
  {
    if (speed > max_speed || sensor_error_count > 10 || !sensorState) phase_braking = true;

    else if (speed == (int)min_speed || speed < (int)min_speed || sensor_error_count == 0) phase_braking = false;

    switch (phase_braking)
    {
      case false: digitalWrite(phase_break_led, LOW); digitalWrite(phase_braking_pin, LOW); break;

      case true:
        digitalWrite(phase_break_led, HIGH);   digitalWrite(phase_braking_pin, HIGH);
        digitalWrite(charging, LOW);  digitalWrite(charging_led, LOW);
        digitalWrite(braking_led, LOW);  digitalWrite(braking_pin, LOW); break;

    }
  }else {

     phase_braking = false; digitalWrite(phase_break_led, LOW); digitalWrite(phase_braking_pin, LOW);
    }

  if (select == 0)
  {
    max_volt = max_min_array[0];
    min_volt = max_min_array[1];
    max_speed = max_min_array[2];
    min_speed = max_min_array[3];
  }

  voltage = bat_volt_cal(30);
  dtostrf(voltage, 4, 1, battery_string);
  char sentdata[100];

  // dtostrf(voltage, 4, 1, voltage_string);
   char brakeState[10];
   char phaseBrakeState[10];
   char fanState[10];
   char temp[10];
   char gerginlikmax[10];
   char gerginlikmin[10];
   char suretmax[10];
   char suretmin[10];
   
  dtostrf(Tc, 4, 1, temp);
  dtostrf(maxVolt,4,1,gerginlikmax);
  dtostrf(minVolt,4,1,gerginlikmin);
  dtostrf(maxSpd,4,1,suretmax);
  dtostrf(minSpd,4,1,suretmin);
  
if(phase_braking!=0)sprintf(phaseBrakeState,"%s","Aciq"); else sprintf(phaseBrakeState,"%s","Bagli");
if(braking!=0)sprintf(brakeState,"%s","Aciq"); else sprintf(brakeState,"%s","Bagli");
if(digitalRead(fan)==HIGH)sprintf(fanState,"%s","Aciq"); else sprintf(fanState,"%s","Bagli");

  sprintf(sentdata, ",batareya_gerginlik=%s,kulek_sureti=%s,tormozlama=%s,faz_tormozlama=%s,batareya_faiz=%s%%,korpu_temperatur=%s,korpu_fan=%s, maxGerginlik=%s | minGerginlik=%s,maxSuret=%s | minSuret=%s",
  battery_string, speed_string,  brakeState, phaseBrakeState,percent_string,temp,fanState,gerginlikmax,gerginlikmin,suretmax,suretmin);
  
  Serial.println(sentdata);
  Serial.print(sensor_error_count);
  Serial2.println(sentdata);
  Serial.print(maxVolt) ; Serial.print(" , "); Serial.print(minVolt); Serial.print(" , ");
  Serial.print(maxSpd) ; Serial.print(" , "); Serial.println(minSpd) ;
  delay(5);
  if (digitalRead(mode_switch) == LOW) voltage_percent = mapfloat(voltage, 12.0, 14.4, 20, 100);
  else voltage_percent = mapfloat(voltage, 24.0, 28.4, 20, 100);

  if (voltage_percent < 0.0) voltage_percent = 0;
  else if (voltage_percent > 100.0) voltage_percent = 100;

  if (voltage_percent > 0 && voltage_percent <= 20)        battery_level = 1;
  else if (voltage_percent > 20 && voltage_percent <= 40)  battery_level = 2;
  else if (voltage_percent > 40 && voltage_percent <= 60)  battery_level = 3;
  else if (voltage_percent > 60 && voltage_percent <= 80)  battery_level = 4;
  else if (voltage_percent > 80 && voltage_percent <= 100) battery_level = 5;
  dtostrf(voltage_percent, 4, 0, percent_string);

  /* BRAKING */
  if (phase_braking) braking = false;
  else if ((voltage > max_volt && sensorState)||(voltage>max_volt&&!wind_sensor)) braking = true;
  else if (voltage == min_volt || voltage < min_volt  ) braking = false;

  if (braking == false)
  {
    digitalWrite(braking_led, LOW); digitalWrite(braking_pin, LOW);
    if (phase_braking == false) digitalWrite(charging_led, HIGH); digitalWrite(charging, HIGH);
  }
  else
  {

    digitalWrite(braking_led, HIGH); digitalWrite(braking_pin, HIGH);
    digitalWrite(charging_led, LOW); digitalWrite(charging, LOW);
  }


  Serial.print("Menu Page: "); Serial.println(menu_page);

  if (menu_page == 0 ) {
    select = 0;
    u8g.firstPage();
    do {
      wind_turbine_performance();
    }
    while (u8g.nextPage());

  }

  else if (menu_page == 1)
  {
    u8g.firstPage();
    do {
      sensorMenu();
    }
    while (u8g.nextPage());
  }

  else if (menu_page == 2)
  {
    u8g.firstPage();
    do {
      drawMenuFull();
    }
    while (u8g.nextPage());
  }

  if (store == 1)
  {
    Serial.println("----------SAVING---------");
    store = 0;
    eeaddress = 0;
    EEPROM.put(eeaddress, max_min_array[0]);
    eeaddress += sizeof(float);
    EEPROM.put(eeaddress, max_min_array[1]);
    eeaddress += sizeof(float);
    EEPROM.put(eeaddress, max_min_array[2]);
    eeaddress += sizeof(float);
    EEPROM.put(eeaddress, max_min_array[3]);
    eeaddress += sizeof(int);
    EEPROM.put(eeaddress, sensor_save);
    eeaddress += sizeof(int);
    EEPROM.put(eeaddress, 1 * (digitalRead(mode_switch) == HIGH));
  }
  if ((time - period) >= 15000) {
    digitalWrite(backlight,HIGH);  //15 saniye erzinde hec bir aktiv olmadigi halda sonsun
    menu_page = 0;
  }
  Serial.print("Wind Sensor: "); Serial.println(wind_sensor);
  flash=!flash;
  wdt_reset();

}


float bat_volt_cal(float limit)
{
  int ivoltage = analogRead(battery_voltage);
  ivoltage=filter_analog1(ivoltage);
  float fvoltage = (ivoltage/1023.0*limit);
  return fvoltage;
}

void menu_clicked()
{ digitalWrite(backlight, LOW); period = time;
  menu_current = 0;
  if ((long)(micros() - last_micros) >= debouncing_time * 1000) {
    if (select != 0) {

      eeaddress = 0;
      EEPROM.get(eeaddress, max_min_array[0]);
      eeaddress += sizeof(float);
      EEPROM.get(eeaddress, max_min_array[1]);
      eeaddress += sizeof(float);
      EEPROM.get(eeaddress, max_min_array[2]);
      eeaddress += sizeof(float);
      EEPROM.get(eeaddress, max_min_array[3]);
    }
    if (menu_page == 0)menu_page = 1;
    else if (menu_page == 1)menu_page = 2;
    else if (menu_page == 2) {
      menu_page = 0;
    }
  }

  last_micros = micros();
}

void up_clicked() {
  digitalWrite(backlight, LOW); period = time;

  if ((long)(micros() - last_micros) >= debouncing_time * 1000) {
    if (select != 0 && menu_page == 2) {
      if (menu_current == 0 || menu_current == 2)max_min_array[menu_current] += 0.1;
      else if (menu_current == 1 && minVolt < maxVolt - 0.2)max_min_array[menu_current] += 0.1;
      else if (menu_current == 3 && minSpd < maxSpd - 0.2)max_min_array[menu_current] += 0.1;

    }
    else if (menu_page == 2 && select == 0)   {
      if (menu_current <= 0) menu_current = MENU_ITEMS - 1; else menu_current--;
    }
  }
  last_micros = micros();
}
/*

*/
void down_clicked()
{
  digitalWrite(backlight, LOW); period = time;
  if ((long)(micros() - last_micros) >= debouncing_time * 1000) {

    if (menu_page == 2 && select != 0)      {
      if (max_min_array[menu_current] > 0.2) {
        if (menu_current == 1 || menu_current == 3) max_min_array[menu_current] -= 0.1;
        else if (menu_current == 0 && minVolt < maxVolt - 0.2) max_min_array[menu_current] -= 0.1;
        else if ((menu_current == 2) && (minSpd < maxSpd - 0.2)) max_min_array[menu_current] -= 0.1;

      }
    }
    else if (menu_page == 2 && select == 0) {
      if (menu_current >= MENU_ITEMS - 1) menu_current = 0; else menu_current++;
    }
  }
  last_micros = micros();
}


void OK_clicked()
{ digitalWrite(backlight, LOW); period = time;
  if ((long)(micros() - last_micros) >= debouncing_time * 1000) {

    if (menu_page == 1) {
      wind_sensor = !wind_sensor;
      sensor_save ^= 1;
      store = 1;
    }
    else if (menu_page == 2) {
      select ^= 1;
      store = 1;
    }

  }
  last_micros = micros();
}

void tempCheck() {

  Vo = analogRead(0);
  R2 = R1 * (1023.0 / (float)Vo - 1.0);
  logR2 = log(R2);
  T = (1.0 / (c1 + c2 * logR2 + c3 * logR2 * logR2 * logR2));
  Tc = T - 273.15;
  Serial.println(Tc);
  (Tc > 35) ? digitalWrite(fan, HIGH) : digitalWrite(fan, LOW);
}


uint16_t filter_analog1(uint16_t input)
{
  static uint16_t result = 0;
  float alfa = 0.7;
  result = result * alfa + input * (1 - alfa);
  return result;
}
